from django.db import models
#from users.models import CustomUser

# Create your models here.




class Schtrip(models.Model):
    owner = models.ForeignKey('auth.User',  on_delete=models.CASCADE)
    #user2 = models.ForeignKey(AppUser, on_delete=models.CASCADE)
    sch_date = models.DateField()
    sch_time = models.TimeField()
    pickup = models.CharField(max_length=200)
    drop = models.CharField(max_length=200)
    startlat = models.DecimalField(max_digits=9,decimal_places=6)
    stoplong = models.DecimalField(max_digits=9, decimal_places=6)
    droplat = models.DecimalField(max_digits=9, decimal_places=6)
    droplong = models.DecimalField(max_digits=9, decimal_places=6)
    rate = models.FloatField()
    distance = models.FloatField(null = True, blank=True)
    expected_fare = models.FloatField(null=True,blank=True)
    timestamp = models.DateField(auto_now=True)
    max_pass = models.IntegerField()
    curr_pass = models.IntegerField(default=0)


 #   people = models.Manager()

class Schtriptrippool(models.Model):
    owner = models.ForeignKey('auth.User',  on_delete=models.CASCADE)
    tripsch = models.ForeignKey(Schtrip, on_delete=models.CASCADE)
    #user2 = models.ForeignKey(AppUser, on_delete=models.CASCADE)
    sch_date = models.DateField()
    sch_time = models.TimeField()
    pickup = models.CharField(max_length=200)
    drop = models.CharField(max_length=200)
    pickuplat = models.DecimalField(max_digits=9,decimal_places=6)
    pickuplong = models.DecimalField(max_digits=9, decimal_places=6)
    droplat = models.DecimalField(max_digits=9, decimal_places=6)
    droplong = models.DecimalField(max_digits=9, decimal_places=6)
    rate = models.FloatField()
    distance = models.FloatField(null = True, blank=True)
    expected_fare = models.FloatField(null=True,blank=True)
    timestamp = models.DateField(auto_now=True)

class CurrentTrip(models.Model):
    tripsch = models.ForeignKey(Schtrip, on_delete=models.CASCADE)
    user1 = models.IntegerField()
    user2 = models.IntegerField()
    user3 = models.IntegerField(null = True)
    user4 = models.IntegerField(null = True)
    user5 = models.IntegerField(null = True)
    user6 = models.IntegerField(null = True)
    user7 = models.IntegerField(null = True)
    user8 = models.IntegerField(null = True)
    user9 = models.IntegerField(null = True)
    user10 = models.IntegerField(null = True)
    start_date = models.DateField(null = True)
    start_time = models.TimeField(null = True)
    lastlat = models.DecimalField(max_digits=9, decimal_places=6)
    lastlong = models.DecimalField(max_digits=9, decimal_places=6)
    last_distance = models.DecimalField(max_digits=9, decimal_places=3,null = True, blank=True)
    timestamp = models.DateField(auto_now=True)
    running = models.BooleanField(default=True)
    curr_passenger = models.IntegerField()

class Finishedtrip(models.Model):
    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    trippcurr = models.ForeignKey(CurrentTrip, on_delete=models.CASCADE)
    sch_date = models.DateField()
    sch_time = models.TimeField()
    pickup_date = models.DateField()
    pickup_time = models.TimeField()
    drop_date = models.DateField()
    drop_time = models.TimeField()
    pickup_loc = models.CharField(max_length=200)
    drop_loc = models.CharField(max_length=200)
    pickuplat = models.DecimalField(max_digits=9,decimal_places=6)
    pickuplong = models.DecimalField(max_digits=9, decimal_places=6)
    droplat = models.DecimalField(max_digits=9, decimal_places=6)
    droplong = models.DecimalField(max_digits=9, decimal_places=6)
    rate = models.FloatField()
    distance = models.DecimalField(max_digits=9, decimal_places=3,null = True, blank=True)
    expected_fare = models.DecimalField(max_digits=9, decimal_places=3,null = True, blank=True)
    collected_fare = models.DecimalField(max_digits=9, decimal_places=3,null = True, blank=True)
    timestamp = models.DateField(auto_now=True)
    passenger = models.IntegerField()


class Finishedtrippool(models.Model):

    owner = models.ForeignKey('auth.User',  on_delete=models.CASCADE)
    tripcurr = models.ForeignKey(CurrentTrip, on_delete=models.CASCADE)
    sch_date = models.DateField()
    sch_time = models.TimeField()
    pickup_date = models.DateField()
    pickup_time = models.TimeField()
    drop_date = models.DateField()
    drop_time = models.TimeField()
    pickup_loc = models.CharField(max_length=200)
    drop_loc = models.CharField(max_length=200)
    pickuplat = models.DecimalField(max_digits=9,decimal_places=6)
    pickuplong = models.DecimalField(max_digits=9, decimal_places=6)
    droplat = models.DecimalField(max_digits=9, decimal_places=6)
    droplong = models.DecimalField(max_digits=9, decimal_places=6)
    rate = models.FloatField()
    distance = models.DecimalField(max_digits=9, decimal_places=3,null = True, blank=True)
    expected_fare = models.DecimalField(max_digits=9, decimal_places=3,null = True, blank=True)
    paid_fare = models.DecimalField(max_digits=9, decimal_places=3,null = True, blank=True)
    timestamp = models.DateField(auto_now=True)

