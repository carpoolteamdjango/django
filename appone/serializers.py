from rest_framework import serializers
from .models import Schtrip,Schtriptrippool,Finishedtrip,Finishedtrippool


##        read_only_fields = ('date_created', 'date_modified')

class SchtripSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schtrip
        fields = ['owner', 'sch_date','sch_time','pickup','drop','startlat','stoplong','droplat','droplong','rate','distance','expected_fare','max_pass']


class SchpoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schtriptrippool
        fields = ['owner', 'tripsch','sch_date','sch_time','pickup','drop','pickuplat','pickuplong','droplat','droplong','rate','distance','expected_fare']

class FinsihedtripSerializer(serializers.ModelSerializer):
    class Meta:
        model = Finishedtrip
        fields = ['owner','sch_date','sch_time','pickup','drop','pickuplat','pickuplong','droplat','droplong','rate','distance','expected_fare']

class gettofromSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schtrip
        fields = ['sch_date','pickup','drop']


