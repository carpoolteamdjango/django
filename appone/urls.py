from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

#from .views import CreateView,DetailsView,finView,getuserView
from .views import  schtripList, finishedtripList,createuser,schtripLista, schpoolList,schtripLista
urlpatterns = {
  #  url(r'^carpool/$', appuserList, name="create"),
#    url(r'^carpoola/(?P<pk>[0-9]+)/$', DetailsView.as_view(), name="get"),
    url(r'^schtrip/$',schtripList, name="create"),
    url(r'^schtripa/$',  schpoolList, name="create"),
    url(r'^getschall/$', schtripLista, name="create"),
    url(r'^createuser/$', createuser.as_view(), name="create"),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),

#    url(r'^getuser/(?P<pk>[0-9]+)/$', getuserView.as_view(), name="get1"),

}

urlpatterns = format_suffix_patterns(urlpatterns)