from rest_framework import generics
from .models import Schtrip,Finishedtrip
from .serializers import SchtripSerializer,FinsihedtripSerializer,SchpoolSerializer
from rest_framework.decorators import api_view,permission_classes
from rest_framework.response import Response
from geopy.distance import distance
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.contrib.auth.models import User

class createuser(APIView):
    #permission_classes = [IsAuthenticated]
    def post(self,request):
        print(self.request.data['username'])
        user = User.objects.create_user(request.data['username'], password=self.request.data['password'])
        user.save()
        return Response({"Hello world from abc"})
# class appuserList(generics.ListCreateAPIView):
    # queryset = AppUser.objects.all()
    # serializer_class = AppUserSerializer
    # def create(self,serializer):
    #     #serializer=AppUserSerializer(request.data)
    #     queryset = self.queryset
    #     serializer = AppUserSerializer(queryset, many=True)
    #     a=serializer.data()
    #     serializer.is_valid()
    #     serializer.save()
    #     return Response({"message": "Hello, world!"})




@api_view(['POST'])
@permission_classes([IsAuthenticated])
def schtripList(request):
    if request.method == 'POST':
        print(request.data)
        #data = {'text': request.data.get('the_post'), 'author': request.user.pk}
        serializer = SchtripSerializer(data=request.data)
        serializer.is_valid()
        plat = request.data['startlat']
        plong = request.data['stoplong']
        dlat = request.data['droplat']
        dlong = request.data['droplong']
        rate = request.data['rate']
        dist = distance((plat,plong),(dlat,dlong)).km
        serializer.validated_data['distance']=dist
        serializer.validated_data['expected_fare'] = float(dist)*float(rate)
        if serializer.is_valid():
            serializer.save()
            #posts = AppUser.object('user'='1')
            return Response([{'expected_fare':serializer.validated_data['expected_fare'],'distance':serializer.validated_data['distance'],'status':'Ok'}])
        return Response(serializer.errors)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def schpoolList(request):
    if request.method == 'POST':
        serializer = SchpoolSerializer(data=request.data)
        trip_id = request.data['tripsch']
        get_data = Schtrip.objects.get(pk=trip_id)
        max_passengers = get_data.max_pass
        curr_passengers = get_data.curr_pass
        expected_fare = get_data.expected_fare
        distance = get_data.distance
        serializer.is_valid()
        serializer.validated_data['expected_fare']=expected_fare/max_passengers
        serializer.validated_data['distance']=distance
        if serializer.is_valid():
            if curr_passengers < max_passengers:
                data = Schtrip.objects.get(pk=trip_id)
                data.curr_pass += 1
                data.save()
                serializer.save()
                return Response({'expected_fare':serializer.validated_data['expected_fare'],'distance':serializer.validated_data['distance'],'status':'Ok'})
            else:
                return Response({'failed': 'max passengers already reached'})
        return Response(serializer.errors)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def schtripLista(request):
    if request.method == 'GET':
        serializer = SchtripSerializer
        date = request.data['sch_date']
        pickup = request.data['pickup']
        drop = request.data['drop']
        posts = Schtrip.objects.filter(pickup=pickup,drop=drop,sch_date=date)
        serializer = SchtripSerializer(posts, many=True)
        return Response(serializer.data)


class finishedtripList(generics.ListCreateAPIView):
    queryset = Finishedtrip.objects.all()
    serializer_class = FinsihedtripSerializer
