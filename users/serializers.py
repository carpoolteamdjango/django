from rest_framework import serializers
from .models import CustomUser
class AddCustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = CustomUser
        fields = ('username', 'email', 'password')
