from django.shortcuts import render
from django.contrib.auth import authenticate
from .models import CustomUser
from .serializers import AddCustomUserSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
@api_view(['GET', 'POST'])
def appuserList(request):
    if request.method == 'GET':
        print(request.data)
        print(request)
        username = request.data['username']
        password = request.data['password']
        user = authenticate(username=username, password=password)
        if not user:
            return Response({'Status':'Authetication_Failed'})
        else:
            token, _ = Token.objects.get_or_create(user=user)
            return Response({'token': token.key},status=HTTP_200_OK)

    elif request.method == 'POST':
        print(request.data)
        serializer = AddCustomUserSerializer(data=request.data)
        serializer.is_valid()
        if serializer.is_valid():
            serializer.save()
            return Response({'status':'ok'})
        return Response(serializer.errors)

